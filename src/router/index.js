import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import LayananKami from "../views/LayananKami.vue";
import TentangKami from "../views/TentangKami.vue";
import Blog from "../views/Blog.vue";
import Mitra from "../views/Mitra.vue";
import Kontak from "../views/Kontak.vue";
import Chat from "../views/chat.vue";
import Kebijakan from "../views/kebijakan.vue";

// direktori layanan
import TukangAc from "../views/layanan/TukangAc.vue";
import TukangListrik from "../views/layanan/TukangListrik.vue";
import TukangPipa from "../views/layanan/TukangPipa.vue";
import TukangKebun from "../views/layanan/tukangkebun.vue";
import TukangCleaning from "../views/layanan/tukangcleaning.vue";
import TukangService from "../views/layanan/tukangservice.vue";
import TukangKunci from "../views/layanan/tukangkunci.vue";
import TukangAtap from "../views/layanan/tukangatap.vue";
import TukangCat from "../views/layanan/tukangcat.vue";
import TukangBangunan from "../views/layanan/tukangbangunan.vue";
import TukangDesainint from "../views/layanan/tukangdesainint.vue";
import TukangGorden from "../views/layanan/tukanggorden.vue";
import TukangCucimobil from "../views/layanan/tukangcucimobil.vue";
import TukangMakeup from "../views/layanan/tukangmakeup.vue";
import TukangNailArt from "../views/layanan/tukangnailart.vue";
import TukangCCTV from "../views/layanan/tukangcctv.vue";


const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: "/",
			name: "home",
			component: Home,
		},
		{
			path: "/layanankami",
			name: "LayananKami",
			component: LayananKami,
		},
		{
			path: "/tentangkami",
			name: "TentangKami",
			component: TentangKami,
		},
		{
			path: "/blog",
			name: "Blog",
			component: Blog,
		},
		{
			path: "/mitra",
			name: "Mitra",
			component: Mitra,
		},
		{
			path: "/kontak",
			name: "Kontak",
			component: Kontak,
		},
		{
			path: "/chat",
			name: "Chat",
			component: Chat,
		},
		{
			path: "/kebijakan",
			name: "Kebijakan",
			component: Kebijakan,
		},

		// Direktori Layanan
		{
			path: "/layanankami/tukangac",
			name: "TukangAc",
			component: TukangAc,
		},
		{
			path: "/layanankami/tukanglistrik",
			name: "TukangListrik",
			component: TukangListrik,
		},
		{
			path: "/layanankami/tukangpipa",
			name: "TukangPipa",
			component: TukangPipa,
		},
		{
			path: "/layanankami/tukangkebun",
			name: "TukangKebun",
			component: TukangKebun,
		},
		{
			path: "/layanankami/tukangcleaning",
			name: "TukangCleaning",
			component: TukangCleaning,
		},
		{
			path: "/layanankami/tukangservice",
			name: "TukangService",
			component: TukangService,
		},
		{
			path: "/layanankami/tukangkunci",
			name: "TukangKunci",
			component: TukangKunci,
		},
		{
			path: "/layanankami/tukangatap",
			name: "TukangAtap",
			component: TukangAtap,
		},
		{
			path: "/layanankami/tukangcat",
			name: "TukangCat",
			component: TukangCat,
		},
		{
			path: "/layanankami/tukangbangunan",
			name: "TukangBangunan",
			component: TukangBangunan,
		},
		{
			path: "/layanankami/tukangdesigninterior",
			name: "TukangDesainint",
			component: TukangDesainint,
		},
		{
			path: "/layanankami/tukanggorden",
			name: "TukangGorden",
			component: TukangGorden,
		},
		{
			path: "/layanankami/tukangcucimobil",
			name: "TukangCucimobil",
			component: TukangCucimobil,
		},
		{
			path: "/layanankami/tukangmakeup",
			name: "TukangMakeup",
			component: TukangMakeup,
		},
		{
			path: "/layanankami/tukangnailart",
			name: "TukangNailArt",
			component: TukangNailArt,
		},
		{
			path: "/layanankami/tukangcctv",
			name: "TukangCCTV",
			component: TukangCCTV,
		},
		

	],
});

export default router;
